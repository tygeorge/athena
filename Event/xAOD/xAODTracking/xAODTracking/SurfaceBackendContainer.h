/*
  Copyright (C) 2002-2023 CERN for the benefit of the ATLAS collaboration
*/

#ifndef XAODTRACKING_SURFACEBACKENDCONTAINER_H
#define XAODTRACKING_SURFACEBACKENDCONTAINER_H

#include "xAODTracking/versions/SurfaceBackendContainer_v1.h"

namespace xAOD {
  typedef SurfaceBackendContainer_v1 SurfaceBackendContainer;
}

#include "xAODCore/CLASS_DEF.h"
CLASS_DEF( xAOD::SurfaceBackendContainer , 1249986989 , 1 )
#endif